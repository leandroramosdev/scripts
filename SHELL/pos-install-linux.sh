#!/usr/bin/env bash
# ----------------------------- VARIÁVEIS ----------------------------- #
PPA_LIBRATBAG="ppa:libratbag-piper/piper-libratbag-git"
PPA_LUTRIS="ppa:lutris-team/lutris"
PPA_GRAPHICS_DRIVERS="ppa:graphics-drivers/ppa"
PPA_PHP="ppa:ondrej/php"
PPA_DELUGE="ppa:deluge-team/stable"

URL_WINE_KEY="https://dl.winehq.org/wine-builds/winehq.key"
URL_PPA_WINE="https://dl.winehq.org/wine-builds/ubuntu/"
URL_GOOGLE_CHROME="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
URL_MYSQL="https://dev.mysql.com/get/mysql-apt-config_0.8.14-1_all.deb"
URL_POL_KEY="http://deb.playonlinux.com/public.gpg"
URL_PPA_POL="http://deb.playonlinux.com/"

DIRETORIO_DOWNLOADS="$HOME/Downloads/programas"

PROGRAMAS_PARA_INSTALAR=(
  snapd
  mint-meta-codecs
  winff
  virtualbox
  flameshot
  ratbagd
  libvulkan1
  libvulkan1:i386
  libgnutls30:i386
  libldap-2.4-2:i386
  libgpg-error0:i386
  libxml2:i386
  libasound2-plugins:i386
  libsdl2-2.0-0:i386
  libfreetype6:i386
  libdbus-1-3:i386
  libsqlite3-0:i386
  php8.1-bcmath
  php8.1-common
  php8.1-enchant
  php8.1-imap
  php8.1-ldap
  php8.1-opcache
  php8.1-readline
  php8.1-sybase
  php8.1-xsl
  php8.1-bz2
  php8.1-curl
  php8.1-fpm
  php8.1-interbase
  php8.1-mbstring
  php8.1-pgsql
  php8.1-snmp
  php8.1-tidy
  php8.1-zip
  php8.1-cgi
  php8.1-dba
  php8.1-gd
  php8.1-intl
  php8.1-mysql
  php8.1-phpdbg
  php8.1-soap
  php8.1-xml
  php8.1-cli
  php8.1-dev
  php8.1-gmp
  php8.1-json
  php8.1-odbc
  php8.1-pspell
  php8.1-sqlite3
  php8.1-xmlrpc
  nginx
  deluge
  insomnia
  curl
  grub-customizer
  htop
  playonlinux
  net-tools
  mysql-server
)
# ---------------------------------------------------------------------- #

# ----------------------------- REQUISITOS ----------------------------- #
## Removendo travas eventuais do apt ##
sudo rm /var/lib/dpkg/lock-frontend
sudo rm /var/cache/apt/archives/lock

## Adicionando/Confirmando arquitetura de 32 bits ##
sudo dpkg --add-architecture i386

## Atualizando o repositório ##
sudo apt update -y

## Adicionando repositórios de terceiros e suporte a Snap (Driver Logitech, Lutris e Drivers Nvidia)##
sudo apt-add-repository "$PPA_LIBRATBAG" -y
sudo add-apt-repository "$PPA_LUTRIS" -y
sudo apt-add-repository "$PPA_GRAPHICS_DRIVERS" -y
sudo apt-add-repository "$PPA_PHP" -y
sudo apt-add-repository "$PPA_DELUGE" -y
wget -nc "$URL_WINE_KEY"
sudo apt-key add winehq.key
sudo apt-add-repository "deb $URL_PPA_WINE bionic main"
wget -nc "$URL_POL_KEY" -O- | sudo apt-key add -
sudo apt-add-repository "deb $URL_PPA_POL bionic main"
# ---------------------------------------------------------------------- #

# ----------------------------- EXECUÇÃO ----------------------------- #
## Atualizando o repositório depois da adição de novos repositórios ##
sudo apt update -y

## Download e instalaçao de programas externos ##
mkdir "$DIRETORIO_DOWNLOADS"
wget -c "$URL_GOOGLE_CHROME"       -P "$DIRETORIO_DOWNLOADS"
wget -c "$URL_MYSQL"               -P "$DIRETORIO_DOWNLOADS"

## Instalando pacotes .deb baixados na sessão anterior ##
sudo dpkg -i $DIRETORIO_DOWNLOADS/*.deb

# Instalar programas no apt
for nome_do_programa in ${PROGRAMAS_PARA_INSTALAR[@]}; do
  if ! dpkg -l | grep -q $nome_do_programa; then # Só instala se não estiver instalado
    apt install "$nome_do_programa" -y
  else
    echo "[INSTALADO] - $nome_do_programa"
  fi
done

sudo apt install --install-recommends winehq-stable wine-stable wine-stable-i386 wine-stable-amd64 -y

## Instalando pacotes Snap ##
sudo snap install spotify
sudo snap install skype --classic
sudo snap install photogimp
sudo snap install discord
sudo snap install vscode --classic
# ---------------------------------------------------------------------- #

# ----------------------------- PÓS-INSTALAÇÃO ----------------------------- #
## Finalização, atualização e limpeza##
sudo apt update && sudo apt dist-upgrade -y
flatpak update
sudo apt autoclean
sudo apt autoremove -y
# ---------------------------------------------------------------------- #
